<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1 class="center">${requestScope.error_title}</h1>
<br>
<p>${requestScope.error_message}</p>
<br>
<p>File exists? <br>${requestScope.file_exists}</p>
</body>
</html>
