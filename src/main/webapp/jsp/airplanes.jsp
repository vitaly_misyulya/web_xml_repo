<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Airplanes</title>
</head>
<body>
<h1>Parsed with '${requestScope.parserType}' parser</h1>

<h1 class="center" align="center"> Passenger planes</h1>

<table border="1" align="center">
    <tr>
        <th>id</th>
        <th>Manufacturer</th>
        <th>Model</th>
        <th>Empty Weight</th>
        <th>Max Range</th>
        <th>Fuel Consumption</th>
        <th>Capacity</th>
    </tr>
    <c:forEach items="${requestScope.passengerPlanes}" var="passPlane">
        <tr>
            <td>${passPlane.idAirplane}</td>
            <td>${passPlane.manufacturer}</td>
            <td>${passPlane.model}</td>
            <td>${passPlane.emptyWeight}</td>
            <td>${passPlane.maxRange}</td>
            <td>${passPlane.fuelConsumption}</td>
            <td>${passPlane.capacity}</td>
        </tr>
    </c:forEach>
</table>

<h1 align="center"> Cargo planes</h1>
<table border="1" align="center">
    <tr>
        <th>id</th>
        <th>Manufacturer</th>
        <th>Model</th>
        <th>Empty Weight</th>
        <th>Max Range</th>
        <th>Fuel Consumption</th>
        <th>Cargo Mass</th>
        <th>Cargo Volume</th>
    </tr>
    <c:forEach items="${requestScope.cargoPlanes}" var="cargoPlane">
        <tr>
            <td>${cargoPlane.idAirplane}</td>
            <td>${cargoPlane.manufacturer}</td>
            <td>${cargoPlane.model}</td>
            <td>${cargoPlane.emptyWeight}</td>
            <td>${cargoPlane.maxRange}</td>
            <td>${cargoPlane.fuelConsumption}</td>
            <td>${cargoPlane.cargoMass}</td>
            <td>${cargoPlane.cargoVolume}</td>
        </tr>
    </c:forEach>
</table>

<h1 align="center"> Fire-fighting planes</h1>

<table border="1" align="center">
    <tr>
        <th>id</th>
        <th>Manufacturer</th>
        <th>Model</th>
        <th>Empty Weight</th>
        <th>Max Range</th>
        <th>Fuel Consumption</th>
        <th>Water Tank Volume</th>
    </tr>
    <c:forEach items="${requestScope.fireFightingPlanes}" var="fireFightingPlane">
        <tr>
            <td>${fireFightingPlane.idAirplane}</td>
            <td>${fireFightingPlane.manufacturer}</td>
            <td>${fireFightingPlane.model}</td>
            <td>${fireFightingPlane.emptyWeight}</td>
            <td>${fireFightingPlane.maxRange}</td>
            <td>${fireFightingPlane.fuelConsumption}</td>
            <td>${fireFightingPlane.waterTankVolume}</td>

        </tr>
    </c:forEach>
</table>
</body>
</html>
