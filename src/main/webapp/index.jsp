<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="en_US" scope="session"/>
<%--<fmt:setLocale value="ru_RU" scope="session"/>--%>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <title><fmt:message key="title.parse-xml" bundle="${rb}"/></title>
</head>
<body>

<div id="container">
    <div id="content">
        <form action="/parse" method="post">
            <fieldset>
                <legend><fmt:message key="legend.choose-parser" bundle="${rb}"/></legend>
                <input id="dom" type="radio" name="parserType" value="DOM" checked="checked">
                <label for="dom"><fmt:message key="parser-type.dom" bundle="${rb}"/></label>

                <input id="sax" type="radio" name="parserType" value="SAX">
                <label for="sax"><fmt:message key="parser-type.sax" bundle="${rb}"/></label>

                <input id="stax" type="radio" name="parserType" value="StAX">
                <label for="stax"><fmt:message key="parser-type.stax" bundle="${rb}"/></label>

                <%--<br>--%>
                <input id="submit" class="center" type="submit" value="<fmt:message key="label.submit" bundle="${rb}"/>"/>
            </fieldset>
        </form>
    </div>
</div>
<div id="footer">
    <ctg:info-time/>
</div>
</body>
</html>

