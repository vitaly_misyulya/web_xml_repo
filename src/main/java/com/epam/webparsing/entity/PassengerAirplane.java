package com.epam.webparsing.entity;

import com.epam.webparsing.util.ResourceManager;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


public class PassengerAirplane extends AbstractAirplane {

    private int capacity;

    public PassengerAirplane() {
    }

    public PassengerAirplane(int capacity) {
        setCapacity(capacity);
    }

    public PassengerAirplane(long idAirplane, AirplaneManufacturer manufacturer, String model, int emptyWeight, int maxRange, double fuelConsumption, int capacity) {
        super(idAirplane, manufacturer, model, emptyWeight, maxRange, fuelConsumption);
        setCapacity(capacity);
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return String.format(ResourceManager.INSTANCE.getString("format.passenger-plane-string"),
                ResourceManager.INSTANCE.getString("passenger-airplane"),
                super.toString(), capacity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PassengerAirplane)) return false;

        PassengerAirplane that = (PassengerAirplane) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(capacity, that.capacity)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(capacity)
                .toHashCode();
    }
}
