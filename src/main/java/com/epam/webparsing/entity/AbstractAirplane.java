package com.epam.webparsing.entity;

import com.epam.webparsing.util.ResourceManager;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;


public abstract class AbstractAirplane {

    private long idAirplane;
    private AirplaneManufacturer manufacturer;
    private String model;
    private int emptyWeight;
    private int maxRange;
    //    measure - litres per kilometer
    private double fuelConsumption;

    public AbstractAirplane() {
    }

    public AbstractAirplane(long idAirplane, AirplaneManufacturer manufacturer, String model, int emptyWeight, int maxRange, double fuelConsumption) {
        setIdAirplane(idAirplane);
        setManufacturer(manufacturer);
        setModel(model);
        setEmptyWeight(emptyWeight);
        setMaxRange(maxRange);
        setFuelConsumption(fuelConsumption);
    }

    public long getIdAirplane() {
        return idAirplane;
    }

    public void setIdAirplane(long idAirplane) {
        this.idAirplane = idAirplane;
    }

    public AirplaneManufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(AirplaneManufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getEmptyWeight() {
        return emptyWeight;
    }

    public void setEmptyWeight(int emptyWeight) {
        this.emptyWeight = emptyWeight;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public String toString() {
        return String.format(ResourceManager.INSTANCE.getString("format.plane-string"),
                idAirplane, manufacturer, model, emptyWeight, maxRange, fuelConsumption);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AbstractAirplane airplane = (AbstractAirplane) o;

        return new EqualsBuilder()
                .append(idAirplane, airplane.idAirplane)
                .append(emptyWeight, airplane.emptyWeight)
                .append(maxRange, airplane.maxRange)
                .append(fuelConsumption, airplane.fuelConsumption)
                .append(manufacturer, airplane.manufacturer)
                .append(model, airplane.model)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(idAirplane)
                .append(manufacturer)
                .append(model)
                .append(emptyWeight)
                .append(maxRange)
                .append(fuelConsumption)
                .toHashCode();
    }

    public static Comparator<? super AbstractAirplane> maxRangeComparator(){
        return Comparator.comparing(AbstractAirplane::getMaxRange);
    }

    public static Comparator<AbstractAirplane> emptyWeightComparator(){
        return Comparator.comparing(AbstractAirplane::getEmptyWeight);
    }

    public static Comparator<AbstractAirplane> fuelConsumptionComparator(){
        return Comparator.comparingDouble(AbstractAirplane::getFuelConsumption);
    }



}
