package com.epam.webparsing.entity;

public enum AirplaneManufacturer {
    AIRBUS,
    ANTONOV,
    BERIEV,
    BOEING,
    BOMBARDIER,
    CESSNA,
    DASSAULT,
    EMBARAER,
    GULFSTREAM,
    SUKHOI,
    TUPOLEV;

    @Override
    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase();
    }
}
