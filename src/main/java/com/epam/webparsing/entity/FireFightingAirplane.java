package com.epam.webparsing.entity;

import com.epam.webparsing.util.ResourceManager;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


public class FireFightingAirplane extends AbstractAirplane {

    private int waterTankVolume;

    public FireFightingAirplane() {
    }

    public FireFightingAirplane(int waterTankVolume) {
        setWaterTankVolume(waterTankVolume);
    }

    public FireFightingAirplane(long idAirplane, AirplaneManufacturer manufacturer, String model, int emptyWeight, int maxRange, double fuelConsumption, int waterTankVolume) {
        super(idAirplane, manufacturer, model, emptyWeight, maxRange, fuelConsumption);
        setWaterTankVolume(waterTankVolume);
    }

    public int getWaterTankVolume() {
        return waterTankVolume;
    }

    public void setWaterTankVolume(int waterTankVolume) {
        this.waterTankVolume = waterTankVolume;
    }

    @Override
    public String toString() {
        return String.format(ResourceManager.INSTANCE.getString("format.firefighting-plane-string"),
                ResourceManager.INSTANCE.getString("firefighting-airplane"),
                super.toString(), waterTankVolume);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof FireFightingAirplane)) return false;

        FireFightingAirplane that = (FireFightingAirplane) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(waterTankVolume, that.waterTankVolume)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(waterTankVolume)
                .toHashCode();
    }
}
