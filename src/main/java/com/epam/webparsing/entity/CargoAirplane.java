package com.epam.webparsing.entity;

import com.epam.webparsing.util.ResourceManager;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


public class CargoAirplane extends AbstractAirplane {

    private int cargoMass;
    private int cargoVolume;

    public CargoAirplane() {
    }

    public CargoAirplane(int cargoMass, int cargoVolume) {
        setCargoMass(cargoMass);
        setCargoVolume(cargoVolume);
    }

    public CargoAirplane(long idAirplane, AirplaneManufacturer manufacturer, String model, int emptyWeight, int maxRange, double fuelConsumption, int cargoMass, int cargoVolume) {
        super(idAirplane, manufacturer, model, emptyWeight, maxRange, fuelConsumption);
        setCargoMass(cargoMass);
        setCargoVolume(cargoVolume);
    }

    public int getCargoMass() {
        return cargoMass;
    }

    public void setCargoMass(int cargoMass) {
        this.cargoMass = cargoMass;
    }

    public int getCargoVolume() {
        return cargoVolume;
    }

    public void setCargoVolume(int cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    @Override
    public String toString() {
        return String.format(ResourceManager.INSTANCE.getString("format.cargo-plane-string"),
                ResourceManager.INSTANCE.getString("cargo-airplane"),
                super.toString(), cargoMass, cargoVolume);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CargoAirplane)) return false;

        CargoAirplane that = (CargoAirplane) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(cargoMass, that.cargoMass)
                .append(cargoVolume, that.cargoVolume)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(cargoMass)
                .append(cargoVolume)
                .toHashCode();
    }
}
