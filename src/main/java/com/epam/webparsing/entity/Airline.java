package com.epam.webparsing.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class Airline {

    private List<AbstractAirplane> planes;
    private String name;

    public Airline() {
        planes = new ArrayList<>();
    }

    public Airline(List<AbstractAirplane> planes, String name) {
        this.planes = planes;
        setName(name);
    }

    public List<AbstractAirplane> getPlanes() {
        return Collections.unmodifiableList(planes);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int numberOfAirplanes() {
        return planes.size();
    }

    public boolean removeAirplane(AbstractAirplane o) {
        return planes.remove(o);
    }

    public boolean containsAllAirplanes(Collection<? extends AbstractAirplane> c) {
        return planes.containsAll(c);
    }

    public AbstractAirplane setAirplane(int index, AbstractAirplane element) {
        return planes.set(index, element);
    }

    public AbstractAirplane getAirplane(int index) {
        return planes.get(index);
    }

    public int indexOfAirplane(Object o) {
        return planes.indexOf(o);
    }

    public boolean retainAllAirplanes(Collection<? extends AbstractAirplane> c) {
        return planes.retainAll(c);
    }

    public boolean addAllAirplanes(int index, Collection<? extends AbstractAirplane> c) {
        return planes.addAll(index, c);
    }

    public void addAirplane(int index, AbstractAirplane element) {
        planes.add(index, element);
    }

    public int lastIndexOfAirplane(Object o) {
        return planes.lastIndexOf(o);
    }

    public void clearAirplanesList() {
        planes.clear();
    }

    public AbstractAirplane removeAirplane(int index) {
        return planes.remove(index);
    }

    public boolean addAllAirplanes(Collection<? extends AbstractAirplane> c) {
        return planes.addAll(c);
    }

    public boolean addAirplane(AbstractAirplane abstractAirplane) {
        return planes.add(abstractAirplane);
    }

    public boolean removeAllAirplanes(Collection<? extends AbstractAirplane> c) {
        return planes.removeAll(c);
    }

    public boolean containsAirplane(Object o) {
        return planes.contains(o);
    }
}
