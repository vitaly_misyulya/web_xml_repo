package com.epam.webparsing.parser;


public enum AirplaneEnum {

    FLYING_STOCK("flying-stock"),
    PASSENGER_AIRPLANE("passenger-airplane"),
    CARGO_AIRPLANE ("cargo-airplane"),
    FIRE_FIGHTING_AIRPLANE ("fire-fighting-airplane"),
    MANUFACTURER("manufacturer"),
    MODEL("model"),
    EMPTY_WEIGHT("empty-weight"),
    MAX_RANGE("max-range"),
    FUEL_CONSUMPTION("fuel-consumption"),
    CAPACITY("capacity"),
    CARGO_MASS("cargo-mass"),
    CARGO_VOLUME("cargo-volume"),
    WATER_TANK_VOLUME("water-tank-volume");

    private String value;

    AirplaneEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
