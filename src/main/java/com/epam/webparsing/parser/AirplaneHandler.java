package com.epam.webparsing.parser;

import com.epam.webparsing.entity.*;
import com.epam.webparsing.util.ResourceManager;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;


public class AirplaneHandler extends DefaultHandler {

    private final String ID_ATTR = ResourceManager.INSTANCE.getString("attr.id");
    private final String AIRLINE_ATTR = ResourceManager.INSTANCE.getString("attr.airline");

    private Airline airline;
    private AbstractAirplane currentAirplane;
    private AirplaneEnum currentEnum;

    public AirplaneHandler() {
        this.airline = new Airline();
    }

    public Airline getAirline() {
        return airline;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (AirplaneEnum.valueOf(localName.replace("-","_").toUpperCase())) {
            case FLYING_STOCK:
                airline.setName(obtainAttributeByName(attributes, AIRLINE_ATTR));
                break;
            case PASSENGER_AIRPLANE:
                currentAirplane = new PassengerAirplane();
                currentAirplane.setIdAirplane(Long.parseLong(obtainAttributeByName(attributes, ID_ATTR).substring(1)));
                break;
            case CARGO_AIRPLANE:
                currentAirplane = new CargoAirplane();
                currentAirplane.setIdAirplane(Long.parseLong(obtainAttributeByName(attributes, ID_ATTR).substring(1)));
                break;
            case FIRE_FIGHTING_AIRPLANE:
                currentAirplane = new FireFightingAirplane();
                currentAirplane.setIdAirplane(Long.parseLong(obtainAttributeByName(attributes, ID_ATTR).substring(1)));
                break;

            default:
                currentEnum = AirplaneEnum.valueOf(localName.replace("-", "_").toUpperCase());
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (AirplaneEnum.valueOf(localName.replace("-","_").toUpperCase())) {
            case PASSENGER_AIRPLANE:
            case CARGO_AIRPLANE:
            case FIRE_FIGHTING_AIRPLANE:
                airline.addAirplane(currentAirplane);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String text = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case MANUFACTURER:
                    currentAirplane.setManufacturer(AirplaneManufacturer.valueOf(text.toUpperCase()));
                    break;
                case MODEL:
                    currentAirplane.setModel(text);
                    break;
                case EMPTY_WEIGHT:
                    currentAirplane.setEmptyWeight(Integer.parseInt(text));
                    break;
                case MAX_RANGE:
                    currentAirplane.setMaxRange(Integer.parseInt(text));
                    break;
                case FUEL_CONSUMPTION:
                    currentAirplane.setFuelConsumption(Double.parseDouble(text));
                    break;
                case CAPACITY:
                    ((PassengerAirplane) currentAirplane).setCapacity(Integer.parseInt(text));
                    break;
                case CARGO_MASS:
                    ((CargoAirplane) currentAirplane).setCargoMass(Integer.parseInt(text));
                    break;
                case CARGO_VOLUME:
                    ((CargoAirplane) currentAirplane).setCargoVolume(Integer.parseInt(text));
                    break;
                case WATER_TANK_VOLUME:
                    ((FireFightingAirplane) currentAirplane).setWaterTankVolume(Integer.parseInt(text));
                    break;
                default:
                    throw new EnumConstantNotPresentException(
                            currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }

    private String obtainAttributeByName(Attributes attributes, String attrName){
        int attrIndex = attributes.getIndex("", attrName);
        return attributes.getValue(attrIndex);
    }
}
