package com.epam.webparsing.parser;

import com.epam.webparsing.exception.TechnicalException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;


public class AirlineSAXBuilder extends AbstractAirlineBuilder {

    private AirplaneHandler airplaneHandler;
    private XMLReader xmlReader;

    public AirlineSAXBuilder() throws TechnicalException {
        super();
        airplaneHandler = new AirplaneHandler();
        try{
            xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(airplaneHandler);
        } catch (SAXException ex){
            throw new TechnicalException("SAX parser error", ex);
        }
    }

    @Override
    public void buildAirline(String fileName) throws TechnicalException {
        try {
            xmlReader.parse(fileName);
        } catch (SAXException ex) {
            throw new TechnicalException("SAX parser error", ex);
        } catch (IOException ex) {
            throw new TechnicalException("IOException for file \"" + fileName + "\".", ex);
        }
        airline = airplaneHandler.getAirline();
    }
}
