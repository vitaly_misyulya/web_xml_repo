package com.epam.webparsing.parser;


import com.epam.webparsing.exception.TechnicalException;

public class AirlineBuilderFactory {

    public static AbstractAirlineBuilder createAirlineBuilder(ParserType parserType) throws TechnicalException {
        switch (parserType) {
            case DOM:
                return new AirlineDOMBuilder();
            case SAX:
                return new AirlineSAXBuilder();
            case STAX:
                return new AirlineStaxBuilder();
            default:
                throw new EnumConstantNotPresentException(parserType.getDeclaringClass(), parserType.name());
        }
    }
}
