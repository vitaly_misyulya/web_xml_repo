package com.epam.webparsing.parser;

import com.epam.webparsing.entity.Airline;
import com.epam.webparsing.exception.TechnicalException;

public abstract class AbstractAirlineBuilder {

    protected Airline airline;

    public AbstractAirlineBuilder() {
        this.airline = new Airline();
    }

    public Airline getAirline() {
        return airline;
    }

    abstract public void buildAirline(String fileName) throws TechnicalException;
}
