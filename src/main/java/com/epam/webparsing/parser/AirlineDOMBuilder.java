package com.epam.webparsing.parser;

import com.epam.webparsing.entity.*;
import com.epam.webparsing.exception.TechnicalException;
import com.epam.webparsing.util.ResourceManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


public class AirlineDOMBuilder extends AbstractAirlineBuilder {

    private final String ID_ATTR = ResourceManager.INSTANCE.getString("attr.id");
    private final String AIRLINE_NAME_ATTR = ResourceManager.INSTANCE.getString("attr.airline");

    private DocumentBuilder documentBuilder;

    public AirlineDOMBuilder() throws TechnicalException {
        airline = new Airline();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            throw new TechnicalException("Error occurred while initializing AirlineDOMBuilder", ex);
        }
    }

    @Override
    public void buildAirline(String fileName) throws TechnicalException {
        Document doc;
        try {
            doc = documentBuilder.parse(fileName);
            Element root = doc.getDocumentElement();

            airline.setName(root.getAttribute(AIRLINE_NAME_ATTR));

            NodeList passengerPlanes = root.getElementsByTagName(AirplaneEnum.PASSENGER_AIRPLANE.getValue());
            NodeList cargoPlanes = root.getElementsByTagName(AirplaneEnum.CARGO_AIRPLANE.getValue());
            NodeList firefightingPlanes = root.getElementsByTagName(AirplaneEnum.FIRE_FIGHTING_AIRPLANE.getValue());

            int length = passengerPlanes.getLength();
            for (int i = 0; i < length; i++) {
                Element planeElement = (Element) passengerPlanes.item(i);
                PassengerAirplane airplane = buildPassengerAirplane(planeElement);
                airline.addAirplane(airplane);
            }

            length = cargoPlanes.getLength();
            for (int i = 0; i < length; i++) {
                Element planeElement = (Element) cargoPlanes.item(i);
                CargoAirplane airplane = buildCargoAirplane(planeElement);
                airline.addAirplane(airplane);
            }

            length = firefightingPlanes.getLength();
            for (int i = 0; i < length; i++) {
                Element planeElement = (Element) firefightingPlanes.item(i);
                FireFightingAirplane airplane = buildFireFightingAirplane(planeElement);
                airline.addAirplane(airplane);
            }
        } catch (IOException ex) {
            throw new TechnicalException("IOException for file \"" + fileName + "\".", ex);
        } catch (SAXException ex) {
            throw new TechnicalException("Parsing failure", ex);
        }
    }

    private PassengerAirplane buildPassengerAirplane(Element planeElement) {
        PassengerAirplane plane = new PassengerAirplane();
        buildAirplane(plane, planeElement);
        plane.setCapacity(Integer.parseInt(getElementTextContent(planeElement, AirplaneEnum.CAPACITY.getValue())));
        return plane;
    }

    private CargoAirplane buildCargoAirplane(Element planeElement) {
        CargoAirplane plane = new CargoAirplane();
        buildAirplane(plane, planeElement);
        plane.setCargoMass(Integer.parseInt(getElementTextContent(planeElement, AirplaneEnum.CARGO_MASS.getValue())));
        plane.setCargoVolume(Integer.parseInt(getElementTextContent(planeElement, AirplaneEnum.CARGO_VOLUME.getValue())));
        return plane;
    }

    private FireFightingAirplane buildFireFightingAirplane(Element planeElement) {
        FireFightingAirplane plane = new FireFightingAirplane();
        buildAirplane(plane, planeElement);
        plane.setWaterTankVolume(Integer.parseInt(getElementTextContent(planeElement, AirplaneEnum.WATER_TANK_VOLUME.getValue())));
        return plane;
    }

    private void buildAirplane(AbstractAirplane plane, Element planeElement) {
        plane.setIdAirplane(Long.parseLong(planeElement.getAttribute(ID_ATTR).substring(1)));
        String manufacturerStr = getElementTextContent(planeElement, AirplaneEnum.MANUFACTURER.getValue());
        plane.setManufacturer(AirplaneManufacturer.valueOf(manufacturerStr.toUpperCase()));
        plane.setModel(getElementTextContent(planeElement, AirplaneEnum.MODEL.getValue()));
        plane.setEmptyWeight(Integer.parseInt(getElementTextContent(planeElement, AirplaneEnum.EMPTY_WEIGHT.getValue())));
        plane.setMaxRange(Integer.parseInt(getElementTextContent(planeElement, AirplaneEnum.MAX_RANGE.getValue())));
        plane.setFuelConsumption(Double.parseDouble(getElementTextContent(planeElement, AirplaneEnum.FUEL_CONSUMPTION.getValue())));
    }

    private String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}
