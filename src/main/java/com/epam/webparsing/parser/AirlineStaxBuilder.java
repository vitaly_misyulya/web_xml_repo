package com.epam.webparsing.parser;

import com.epam.webparsing.entity.*;
import com.epam.webparsing.exception.TechnicalException;
import com.epam.webparsing.util.ResourceManager;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class AirlineStaxBuilder extends AbstractAirlineBuilder {

    private final String ID_ATTR = ResourceManager.INSTANCE.getString("attr.id");
    private final String AIRLINE_NAME_ATTR = ResourceManager.INSTANCE.getString("attr.airline");

    private XMLInputFactory inputFactory;

    public AirlineStaxBuilder() {
        this.inputFactory = XMLInputFactory.newInstance();
    }

    @Override
    public void buildAirline(String fileName) throws TechnicalException {
        XMLStreamReader reader;
        String elementTag;

        try (FileInputStream inputStream = new FileInputStream(new File(fileName))) {
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    elementTag = reader.getLocalName();
                    switch (AirplaneEnum.valueOf(elementTag.replace("-", "_").toUpperCase())) {
                        case FLYING_STOCK:
                            airline.setName(reader.getAttributeValue("", AIRLINE_NAME_ATTR));
                            break;
                        case PASSENGER_AIRPLANE:
                            airline.addAirplane(buildAirplane(new PassengerAirplane(), reader));
                            break;
                        case CARGO_AIRPLANE:
                            airline.addAirplane(buildAirplane(new CargoAirplane(), reader));
                            break;
                        case FIRE_FIGHTING_AIRPLANE:
                            airline.addAirplane(buildAirplane(new FireFightingAirplane(), reader));
                            break;
                    }
                }
            }
        } catch (XMLStreamException ex) {
            throw new TechnicalException("StAX parsing error!", ex);
        } catch (FileNotFoundException ex) {
            throw new TechnicalException("File " + fileName + " not found!", ex);
        } catch (IOException ex) {
            throw new TechnicalException("Unable to close file " + fileName, ex);
        }
    }

    private AbstractAirplane buildAirplane(AbstractAirplane plane, XMLStreamReader reader) throws XMLStreamException {
        String elementTag;
        plane.setIdAirplane(Long.parseLong(reader.getAttributeValue("", ID_ATTR).substring(1)));
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    elementTag = reader.getLocalName();
                    switch (AirplaneEnum.valueOf(elementTag.replace("-", "_").toUpperCase())) {
                        case MANUFACTURER:
                            plane.setManufacturer(AirplaneManufacturer.valueOf(getXMLText(reader).toUpperCase()));
                            break;
                        case MODEL:
                            plane.setModel(getXMLText(reader));
                            break;
                        case EMPTY_WEIGHT:
                            plane.setEmptyWeight(Integer.parseInt(getXMLText(reader)));
                            break;
                        case MAX_RANGE:
                            plane.setMaxRange(Integer.parseInt(getXMLText(reader)));
                            break;
                        case FUEL_CONSUMPTION:
                            plane.setFuelConsumption(Double.parseDouble(getXMLText(reader)));
                            break;
                        case CAPACITY:
                            ((PassengerAirplane) plane).setCapacity(Integer.parseInt(getXMLText(reader)));
                            break;
                        case CARGO_MASS:
                            ((CargoAirplane) plane).setCargoMass(Integer.parseInt(getXMLText(reader)));
                            break;
                        case CARGO_VOLUME:
                            ((CargoAirplane) plane).setCargoVolume(Integer.parseInt(getXMLText(reader)));
                            break;
                        case WATER_TANK_VOLUME:
                            ((FireFightingAirplane) plane).setWaterTankVolume(Integer.parseInt(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    elementTag = reader.getLocalName();
                    if (elementTag.equals(AirplaneEnum.PASSENGER_AIRPLANE.getValue())
                            | elementTag.equals(AirplaneEnum.CARGO_AIRPLANE.getValue())
                            | elementTag.equals(AirplaneEnum.FIRE_FIGHTING_AIRPLANE.getValue())) {
                        return plane;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Airplane");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
