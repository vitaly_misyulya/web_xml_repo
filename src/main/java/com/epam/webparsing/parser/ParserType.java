package com.epam.webparsing.parser;


public enum ParserType {
    SAX, DOM, STAX;
}
