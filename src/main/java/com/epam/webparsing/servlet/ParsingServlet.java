package com.epam.webparsing.servlet;

import com.epam.webparsing.creator.AirlineCreator;
import com.epam.webparsing.entity.*;
import com.epam.webparsing.exception.TechnicalException;
import com.epam.webparsing.parser.ParserType;
import com.epam.webparsing.util.ResourceManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@javax.servlet.annotation.WebServlet(urlPatterns = "/parse")
public class ParsingServlet extends javax.servlet.http.HttpServlet {

    private static final String XML_FILE_PATH = ResourceManager.INSTANCE.getString("xml-file-path");

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Logger logger = LogManager.getLogger(ParsingServlet.class);
        String parserStr = request.getParameter("parserType");
        logger.info("[ParsingServletTAG] parserType: " + parserStr);
       /* logger.debug("parserType: " + parserStr);
        logger.error("parserType: " + parserStr);*/
        ParserType parserType = ParserType.valueOf(parserStr.toUpperCase());

        request.setAttribute("parserType", parserStr);

        String rootPath = System.getProperty("root_path");
        String pathToXml = rootPath + XML_FILE_PATH;
//        String pathToXml = XML_FILE_PATH;

        logger.info(String.format("rootPath:%s, xmlPath:%s", rootPath, pathToXml));

        Airline airline;

        try {
            airline = AirlineCreator.createAirlineFromXml(parserType, pathToXml);
        } catch (TechnicalException e) {

//            response.sendError(404, "[Message] " + e.getMessage());

            request.setAttribute("error_title", ResourceManager.INSTANCE.getString("parsing-error"));
            request.setAttribute("error_message", e.getMessage());
            request.getRequestDispatcher("jsp/error.jsp").forward(request, response);
            return;
        }

        List<AbstractAirplane> planes = airline.getPlanes();

        List<PassengerAirplane> passengerAirplanes = planes.stream()
                .filter(p -> p instanceof PassengerAirplane)
                .map(p -> (PassengerAirplane) p)
                .collect(Collectors.toCollection(ArrayList::new));

        List<CargoAirplane> cargoAirplanes = planes.stream()
                .filter(p -> p instanceof CargoAirplane)
                .map(p -> (CargoAirplane) p)
                .collect(Collectors.toCollection(ArrayList::new));

        List<AbstractAirplane> fireFightingAirplanes = planes.stream()
                .filter(p -> p instanceof FireFightingAirplane)
                .collect(Collectors.toCollection(ArrayList::new));

        request.setAttribute("planes", planes);
        request.setAttribute("passengerPlanes", passengerAirplanes);
        request.setAttribute("cargoPlanes", cargoAirplanes);
        request.setAttribute("fireFightingPlanes", fireFightingAirplanes);
        request.getRequestDispatcher("jsp/airplanes.jsp").forward(request, response);


    }
}
