package com.epam.webparsing.creator;

import com.epam.webparsing.entity.Airline;
import com.epam.webparsing.exception.TechnicalException;
import com.epam.webparsing.parser.AbstractAirlineBuilder;
import com.epam.webparsing.parser.AirlineBuilderFactory;
import com.epam.webparsing.parser.ParserType;


public class AirlineCreator {

    /**
     * Creates airline parsing xml file with parser defined by parserType
     *
     * @param parserType type of parser to parse xml
     * @param filePath   path to xml file
     * @return Airline that was parsed from xml file
     */
    public static Airline createAirlineFromXml(ParserType parserType, String filePath) throws TechnicalException {
        AbstractAirlineBuilder builder = AirlineBuilderFactory.createAirlineBuilder(parserType);
        builder.buildAirline(filePath);
        return builder.getAirline();
    }
}
