package com.epam.webparsing.run;

import com.epam.webparsing.creator.AirlineCreator;
import com.epam.webparsing.entity.AbstractAirplane;
import com.epam.webparsing.entity.Airline;
import com.epam.webparsing.exception.TechnicalException;
import com.epam.webparsing.parser.ParserType;
import com.epam.webparsing.util.ResourceManager;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class AirlineRunner {

    private static final String XML_FILE_PATH = ResourceManager.INSTANCE.getString("xml-file-path");


    public static void main(String[] args) {

        ResourceManager resourceManager = ResourceManager.INSTANCE;

        System.out.println(resourceManager.getString("report.parser-type"));

//        ReportText reportText = new ReportText();

        ParserType parserType = ParserType.STAX;

        Airline airline = null;

        try {
            airline = AirlineCreator.createAirlineFromXml(parserType, XML_FILE_PATH);
        } catch (TechnicalException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println(airline.getName());

        /*

        reportText.appendString(resourceManager.getString("report.parser-type") + parserType);
        reportText.appendAirline(airline);

        ArrayList<AbstractAirplane> planes = new ArrayList<>(airline.getPlanes());
        Collections.sort(planes, AbstractAirplane.maxRangeComparator());
        planes.sort(AbstractAirplane.maxRangeComparator());

        reportText.appendString(resourceManager.getString("report.planes-sorted"));
        reportText.appendAirplaneList(planes);

        double minFuelConsumption = 5.;
        double maxFuelConsumption = 15.;

        List<AbstractAirplane> filtered = new AirplanesFilter(airline)
                .addMinFuelConsumption(minFuelConsumption)
                .addMaxFuelConsumption(maxFuelConsumption)
                .filter();

        reportText.appendString(resourceManager.getString("report.planes-fuel_consumption") + minFuelConsumption + ", " + maxFuelConsumption + "]:");
        reportText.appendAirplaneList(filtered);

        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        reportText.writeReportToFile(REPORT_FILE_PATH + ldt.format(dateTimeFormatter) + REPORT_FILE_EXT);*/
    }
}
